# Jason Brown
# 19/04/21
# Symbolic Dice

from jb_misc_lib.core import choose_from_list, printv
from functools import reduce

class SymbolicDice():
    def __init__(self, prime_quanta: set, symbol_table: dict, dice_definitions: dict):
        # Prime_quanta given as 'Foo|Bar' means call it 'Foo' when +ve and 'Bar' when -ve
        # Symbol table and dice definitions keys should be single characters
        # These keys are case-sensitive and should be mutually exclusive
        # Proposed convention is lower case for symbols, upper for dice
        # For an example of how to structure these arguments see the SWRPG object below
        self.pq = prime_quanta
        self.st = symbol_table
        self.dd = dice_definitions

    @staticmethod
    def roll_result_to_str(roll_result):
        results = []
        for k, v in roll_result.items():
            single_name = len(x := k.split('|')) == 1
            quanta_name = k if single_name else x[v < 0]  # First for +ve, second for -ve
            value = v if single_name else abs(v)
            if value != 0:
                results.append(f'{quanta_name}: {value}')
        return '\n'.join(results)


    def roll(self, roll_string, return_as_string=True, verbose=False):
        # Roll string contains letters corresponding to dice or prime quanta
        # May aslo have numbers denoting quantity

        # Convert numbers of symbols into symbols
        expanded_str = ""
        num_str = ""
        for _char in roll_string:
            if _char.isdigit():
                num_str += _char
            else:
                expanded_str += int(num_str) * _char if num_str != "" else _char
                num_str = ""

        printv(f'Expanded roll string: {expanded_str}', verbose_flag=verbose)

        # Swap dice rolls for their results
        result_str = ''.join([choose_from_list(self.dd[_char]) if _char in self.dd.keys() else _char
                             for _char in expanded_str])

        printv(f'Roll symbols: {result_str}', verbose_flag=verbose)

        # Convert from symbols to prime quanta and sum
        roll_result = {k:0 for k in self.pq}
        for symbol in result_str:
            for pq_pair in self.st[symbol]:
                roll_result[pq_pair[0]] += pq_pair[1]

        printv(f'Roll result: {roll_result}', verbose_flag=verbose)

        return self.roll_result_to_str(roll_result) if return_as_string else roll_result


swrpg_dice = SymbolicDice(
    {
        "Triumph",
        "Despair",
        "Success|Failure",
        "Advantage|Threat",
        "White Force",
        "Black Force"
    },
    {
        "t": [("Triumph", 1), ("Success|Failure", 1)],
        "s": [("Success|Failure", 1)],
        "a": [("Advantage|Threat", 1)],
        "d": [("Despair", 1), ("Success|Failure", -1)],
        "f": [("Success|Failure", -1)],
        "h": [("Advantage|Threat", -1)],
        "w": [("White Force", 1)],
        "b": [("Black Force", 1)]
    },
    {
        "P": ["", ["s", 2], ["ss", 2], "a", ["sa", 3], ["aa", 2], "t"],
        "A": ["", ["s", 2], "ss", ["a", 2], "sa", "aa"],
        "B": [["", 2], "s", "sa", "aa", "a"],
        "C": ["", ["f", 2], ["ff", 2], ["h", 2], ["fh", 2], ["hh", 2], "d"],
        "D": ["", "f", "ff", ["h", 3], "hh", "fh"],
        "S": [["", 2], ["f", 2], ["h", 2]],
        "F": [["b", 6], "bb", ["w", 2], ["ww", 3]]
    }
)

