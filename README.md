# Discord Dice Bot

A discord bot designed to aid with dice rolling.
Currently built around DnD style dice rolling but will eventually expand to include others.

## Setup

### Add the existing bot

If you just want to use the bot then simply click the link below to add the bot to your server.
This bot is not permanently hosted so will not always be online.
For a more permanent solution it's better to build the bot yourself (see creating a new bot).

https://discord.com/api/oauth2/authorize?client_id=809189914158825502&permissions=519232&scope=bot

### Creating a new bot

This will allow you to host the bot yourself or on a dedicated server

1. Clone this repo
2. Install the requirements (discord.py, numpy and jb-misc-lib)
3. Create a discord bot using the discord website
4. Create a file called 'auth.py' and write in it "TOKEN='your_bot_token'", where your_bot_token is the token for your discord bot. Also add "ADMIN_PASSWORD='some_password'" to allow you to authenticate as admin and gain access to some powerful functionality.
5. Run main.py to host the bot on your machine (or upload files and run on a cloud server)
6. Using an invite link from your discord bot, invite it to whatever server you want it on with the correct permissions

## Usage

To use the bot type -db and then your command, the main commands are listed below

### help

'-db help' will bring up some basic usage information.

### roll

'-db roll roll_string' will make the bot evaluate roll_string and give you the output.
Roll strings are taken and any dice rolls contained within them (denoted xdp where x is the number of times to roll a polyhedral die with p sides) are evaluated before the whole expression is evaluated as python. roll_strings cannot have spaces as each set of characters broken by spaces is interpreted as a separate roll_string.

#### Multiple rolls of the same string

Putting xl where x is an integer, before your dice roll will make evaluated that roll x times. Putting a t before that will cause the bot to make all those rolls and then add up their results before giving you a total.

#### Conditional dice rolls

Conditional dice rolls may be done with the following syntax, without the brackets:
(roll_string)(comparator)(roll_string):(roll_string)?(roll_string)

The first two roll_strings are compared against each other using the comparator which can be any of the following: ==, >, <, <=, >=, !=

Note the use of == rather than =

If the result of the comparison is true then the value of the third roll string is computed and returned, if false then the fourth.
If no ? and final roll_string is given, the bot assumes ?0 which means if it evaluates false it returns 0.

Values of the other roll strings can be accessed in the last 2 roll strings via the use of the characters v, c, t and f. These stand for value, condition, true and false and represent the 4 roll_strings in order.
Note you cannot access the value of true from true, nor the same with false.
You also cannot access both false in true and true in false as this would create a loop.

You cannot nest conditional rolls (yet).

#### Inline python

You can make use of the python functions int, max and min within your roll_string though remember to not use spaces.

#### Examples:

Roll 3, 6 sided dice and add 9 to the result: '-db roll 3d6+9'

Roll 10d4 and add it to 3d6 which has been divided by 2 and rounded down: '-db roll 10d4+int(3d6/2)'

Roll a d20 and then roll a d4: '-db roll d20 d4'

Roll 3 lots of 4d20: '-db roll 3l4d20'

Roll 2 lots of 2d5+6 and total the result: '-db roll t2l2d5+6'

Roll a d20+9 and if it's greater than 16 roll d10+4: '-db roll d20+9>16:d10+4'

Roll a d20+5 and if it's higher than or equal to 12 roll 2d8, if lower halve the 2d8 roll: '-db roll d20+5>=12:2d8?t/2'
