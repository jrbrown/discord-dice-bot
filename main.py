# 10/02/2021
# Jason Brown
# Main file for discord bot


from auth import TOKEN
import discord

from importlib import reload

from src.command_handler import CommandHandler
from jb_misc_lib.core import get_time_stamp


if __name__ == '__main__':
    client = discord.Client()
    cmd_handler = CommandHandler(client)

    @client.event
    async def on_ready():
        print(f'We have logged in as {client.user}')


    @client.event
    async def on_message(message):

        # Make some data easier to access
        guild = message.guild
        channel = message.channel
        author = message.author
        message_text = message.content
        time_stamp = get_time_stamp()

        if author == client.user:
            return

        # Output info on any message received for debugging purposes
        print(f'\n{author} in {channel} channel in {guild} guild at {time_stamp}:')
        print(f"Message obj: {message}")
        print(f"Content: {message_text}\n")

        # Check to clear message_prepend
        if 'clear_prepend' in message_text and channel.id in cmd_handler.message_prepends.keys():
            del cmd_handler.message_prepends[channel.id]

        # Check and reload message_prepend
        if channel.id in cmd_handler.message_prepends.keys() and message_text[:3] != '-db':
            message_text = cmd_handler.message_prepends[channel.id] + message_text
            print(f"Message content prepended to: {message_text}\n")

        message_text = message_text.split(' ')

        # If bot is called
        if message_text[0].lower() == '-db':

            command_args = []

            # Get the command_str
            if len(message_text) == 1:
                await cmd_handler.ext_send(f'Hi there {author}, confused? Here\'s some help:', channel=channel, guild=guild)
                command_str = 'help'

            else:
                command_str = message_text[1].lower()
                if len(message_text) > 2:
                    command_args = message_text[2:]

            # Call the command and output results
            await cmd_handler.command(*command_args, guild=guild, channel=channel, author=author, command_str=command_str)

    client.run(TOKEN)
