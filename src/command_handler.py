# 14/02/2021
# Jason Brown
# Session class


from collections import defaultdict

from src.dice import roll as dice_roll
from jb_misc_lib.core import choose_from_list
from auth import ADMIN_PASSWORD


"""
NOTE! For all commands the first two args provided will be the author's name and then the text they used to call the
command
"""


class CommandHandler:
    def __init__(self, client=None):
        self.admins = {}
        self.message_prepends = {}
        self.quiet_fail_channels = {}
        self.client = client

        self.c_guild = None
        self.c_channel = None
        self.c_author = None
        self.c_command_str = None
        self.c_name = None

        self.command_table = {
            "help": self.bot_help,
            "hello": self.greeting,
            "hi": self.greeting,
            "roll": self.roll,
            "mroll": self.mroll,
            "set_prepend": self.set_prepend,
            "clear_prepend": self.clear_prepend,
            "quiet_fail": self.quiet_fail,
            "fail_quiet": self.quiet_fail,
            "loud_fail": self.loud_fail,
            "fail_loud": self.loud_fail,
            "make_admin": self.make_admin,
            "check_admin": self._check_admin,
            "remove_admin": self.remove_admin,
            "clear_admins": self.clear_admins,
            "execute": self.execute,
            "debug": self.debug
        }


    @staticmethod
    async def ext_send(msg, channel, guild=None):
        if channel is not None:
            print(f"Bot output in channel {channel} in guild {guild}:\n{msg}\n")
            await channel.send(msg)
            print(f"Bot output end\n")
        else:
            raise Exception(f"Error, no channel found for message:\n{msg}\n")


    async def send(self, msg):
        await self.ext_send(msg=msg, channel=self.c_channel, guild=self.c_guild)


    async def command(self, *args, guild, channel, author, command_str):

        command_str = command_str.lower()

        if author.id not in self.admins.keys():
            self.admins[author.id] = False

        if channel.id not in self.quiet_fail_channels.keys():
            self.quiet_fail_channels[channel.id] = False

        # Store current stuff
        self.c_author = author
        self.c_channel = channel
        self.c_guild = guild
        self.c_command_str = command_str
        self.c_name = author.nick if author.nick is not None else author.name

        if "clear_prepend" in args:
            await self.clear_prepend()

        if command_str not in self.command_table.keys():
            if not self.quiet_fail_channels[channel.id]:
                await self.bot_help("unknown_command")
        else:
            await self.command_table[command_str](*args)

        # Reset store
        self.c_guild = None
        self.c_channel = None
        self.c_author = None
        self.c_command_str = None
        self.c_name = None


    async def bot_help(self, *args):
        help_str = """To call the bot type '-db'
        
To make a roll use the roll command and then specify the roll to make
For example to roll 2 d8's and add 5: '-db roll 2d8+5'

For in depth help type '-db help more'
For examples type '-db help examples'

To set a prepend (this text will be assumed to be in-front of any message, unless it beings '-db'):
'-db set_prepend text'

To remove any prepend just type 'clear_prepend'

To make the bot fail quietly or loudly (Whether or not to respond if a roll fails or command not recognised):
'-db quiet_fail'
'-db loud_fail'"""

        help_more = ["""To roll use the following command structure:
'-db roll roll_string' where roll_string defines the roll to be evaluated

Multiple roll_strings can be used with mroll command and each will be evaluated in turn:
'-db mroll roll_string1 roll_string2'

The following shows the structure of roll_strings symbolically

General roll_string: tnla
    a: The general roll string. ydz will roll a polyhedral dice with sides z, y times.
        When using d, the dice rolls are summed, to multiply them use p instead.
        If y is not given it is assumed to be 1, d and p function identically in this case.
        Other than that everything is evaluated by eval
    nl: Optional, Whether to make multiple rolls of a. n is the number of rolls.
    t: Optional, for use with nl. Including it will sum the total output of that string
        omitting it will print each l roll separately
        
Conditional roll_string: tnlv>c:t'?f'
    tnl are as before, they apply to the roll as a whole. (Leading t only)
    v: The first roll string
    c: The second roll string
    >: The condition for roll strings v and c. Evaluated with python (So use == not = for equals)
    t': The roll string to be evaluated if condition evaluated to true
    f': The roll string to be evaluated if condition evaluated to false""",
                     """
    v, c, t, and f can be used in the expressions of t' and f' to use those results.
    Cannot use t in t' or f in f'.
    Cannot use both t in f' and f in t'.
    If no ?f' provided, assumes ?0
    For example, roll a d20 and if above 16 roll a d8 else use half the d8 value:
        d20>16:d8?t/2
        
Fixed values and arrays:
    If multiple lots of a roll_string are being evaluated (i.e n>1 for nl) then we can fix and array rolls. Fixed rolls are calculated once and their value is substituted for all the resulting rolls
        NOTE: Only the final value of a fixed roll will appear in the 'roll explanation'
    Arrays can be used to have slightly different rolls across the multiple rolls. Array elements MUST equal the number of rolls for that string, arrays can't be nested or overlap.

    xydz is the syntax for fixed rolls where x denotes fixed and as before ydz means roll polyhedral with z sides, y times and sum the result. Using p instead of d multiplies the result.

    [a1,a2,...] is the general form of an array where a1, a2, etc. are different roll_strings for each roll.
        Example: 3ld20+[3,5,6], equivalent to d20+3 d20+5 d20+6"""]

        help_examples = """The following is a set of examples that show the intention, the command and a possible output.

Roll a d20 and add 5 to the result:
    -'db roll d20+5'
        13 (8+5)

Roll a d20+7 and then roll a d6
    '-db roll d20+7 d6'
        19 (12+7)
        4

Roll two d20's and add 5 to the highest, then multiply that by 2
    -'db roll (max(d20,d20)+5)\*5'
        32 ((max(11,6)+5)\*2)

Roll a d20+9 and if it's higher than or equal to 16 roll a d10+4
    '-db roll d20+9>=16:d10+4'
        14 {18 (9+9) >= 16: 14 (10+4)}

Roll a d20+5 and if it's greater than 12 roll 4d8 and if not return half of the 4d8's value, rounded down
    '-db roll d20+5>12:4d8?int(t/2)'
        6 {6 (1+5) > 12: 6 (int(12/2))}

Roll 3 lots of d20+3
    '-db roll 3ld20+3'
        16 (13+3)
        5 (2+3)
        21 (18+3)

Roll a d20 and if it's greater than 10 roll a d8, do this 3 times and sum the results of the d8's
    '-db roll t3ld20>10:d8'
        6 [0 {10 > 10: 0}, 5 {15 > 10: 5}, 1 {12 > 10: 1}]
        
Roll a d20 3 times with 3 different modifiers, for each roll if it's greater than 12 return the value of an initial 4d8 roll that is the same for all rolls
    '-db roll 3ld20+[1,3,7]>12:x4d8'
        19 {20 (19+1) > 12: 19}
        0 {8 (5+3) > 12: 0}
        19 {22 (15+7) > 12: 19}
    """

        help_unknown_command = f"""Sorry but \'{self.c_command_str}\' is not a recognised command

{help_str}"""

        help_str_dict = defaultdict(lambda: help_str)
        help_str_dict["more"] = help_more
        help_str_dict["examples"] = help_examples
        help_str_dict["unknown_command"] = help_unknown_command

        if len(args) == 1:
            arg = args[0].lower()
            help_text = help_str_dict[arg]
            if isinstance(help_text, str):
                await self.send(help_text)
            else:
                for x in help_text:
                    await self.send(x)
        else:
            await self.send(help_str_dict[None])


    async def greeting(self, *args):
        normal_greetings = [
            [f"Hi there {self.c_name}", 1],
            [f"Hello {self.c_name}", 1],
            [f"Greetings {self.c_name}", 0.5],
            [f"Good day to you {self.c_name}", 0.3]
        ]

        prequel_greetings = [
            ["GENERAL KENOBI!", 1],
            ["*Shy beeping*", 0.2]
        ]

        lowercase_args = [self.c_command_str] + [x.lower() for x in args]

        if lowercase_args == ['hello', 'there']:
            await self.send(choose_from_list(prequel_greetings))
        else:
            await self.send(choose_from_list(normal_greetings))


    async def mroll(self, *args):
        # Check roll was given
        if not args:
            if not self.quiet_fail_channels[self.c_channel.id]:
                await self.send(f'{self.c_name}, please provide a roll string')
            return

        output = f'Roll for {self.c_name}:\n'
        made_a_successful_roll = False
        exceptions = []

        # We'll treat every string provided as a roll string, if we get errors we'll print to console
        for _string in args:
            try:
                roll_output = dice_roll(_string)
            except Exception as e:
                exceptions.append(e)
                print(f'\nAn exception occurred in roll:\n{e}')
            else:
                for roll_result in roll_output:
                    if roll_result[1] is not None:
                        output += f'\t{roll_result[1]}\n'
                        made_a_successful_roll = True
                        if roll_result[0] == 69:
                            output += '\t\tNice.\n'

        # Check there were valid rolls
        if not made_a_successful_roll:
            if not self.quiet_fail_channels[self.c_channel.id]:
                await self.send('Sorry but no working roll string was found')
        else:
            await self.send(output)

        if exceptions and not self.quiet_fail_channels[self.c_channel.id]:
            await self.send(f'Some errors cropped up in your roll strings, {self.c_name}:')
            for e in exceptions:
                await self.send(', '.join(e.args))


    async def roll(self, *args):
        string = ''.join(args)
        await self.mroll(string)


    async def set_prepend(self, *args):
        self.message_prepends[self.c_channel.id] = ' '.join(args) + ' '
        await self.send(f'Prepend \'{self.message_prepends[self.c_channel.id]}\' set')


    async def clear_prepend(self):
        await self.send(f'Prepend \'{self.message_prepends[self.c_channel.id]}\' cleared')
        del self.message_prepends[self.c_channel.id]


    async def quiet_fail(self):
        self.quiet_fail_channels[self.c_channel.id] = True
        await self.send('Ok, I\'ll be quieter now')


    async def loud_fail(self):
        self.quiet_fail_channels[self.c_channel.id] = False
        await self.send('Ok, I\'ll alert you when I get confused')


    async def make_admin(self, password):
        if password == ADMIN_PASSWORD:
            self.admins[self.c_author.id] = True
            await self.send(f'{self.c_author} given admin rights')
        else:
            await self.send('Incorrect password for admin rights')


    async def remove_admin(self):
        if await self.check_admin():
            self.admins[self.c_author.id] = False
            await self.send(f'{self.c_author} no longer has admin rights')


    async def clear_admins(self):
        if await self.check_admin():
            for auth_id in self.admins.keys():
                self.admins[auth_id] = False
            await self.send('All admins cleared')


    async def _check_admin(self):
        await self.check_admin(silent_on_true=False)


    async def check_admin(self, silent_on_true=True):
        if self.admins[self.c_author.id]:
            if not silent_on_true:
                await self.send(f'{self.c_author} has admin rights')
            return True
        else:
            await self.send(f'{self.c_author} does not have admin rights')
            return False


    async def execute(self, *args):
        if await self.check_admin():
            if args[0] == 'await':
                code = ' '.join(args[1:])
                await eval(code)
            else:
                code = ' '.join(args)
                eval(code)


    async def debug(self, *args):
        arg_str = ' '.join(args)
        arg_str = f'await self.send({arg_str})'
        await self.execute(*arg_str.split(' '))
