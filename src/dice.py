# 22/01/2021
# Jason Brown
# Dice rolling functions


import numpy as np

from jb_misc_lib.core import printv, ensure_list, flatten

letter_operator_dict = {
    "d": ["+", ["+", "("], ["+", "-", ")"]],
    "p": ["*", ["*", "+", "-", "("], ["*", "/", "+", "-", ")"]]
}

legal_characters = 'tldpfcvminax<>=!+-/*%()[],:?'


def roll_poly_n(poly, n=1):
    rolls = np.random.randint(1, poly+1, n)
    return np.sum(rolls), rolls


def check_letter(letter, containing_string):
    string_split = containing_string.split(letter)

    if string_split[0] == '':
        string_split = string_split[1:]

    letter_count = len(string_split)

    if letter_count > 2:
        raise Exception(f"Too many {letter}'s in {containing_string}")

    elif letter_count == 2:
        try:
            prefix = int(string_split[0])
            suffix = string_split[1]

        except ValueError:
            raise Exception(f"Characters before {letter} in {containing_string} could not be converted to an integer")

    else:
        prefix = 1
        suffix = string_split[0]

    return prefix, suffix


def roll_core(roll_string, auto_bracket_dice=True, debug=False):
    # We take the string and evaluate any rolls, reinsert values back into the string and the evaluate it
    # If auto_bracket_dice we treat dice rolls as bracketed entities
    # e.g. 2*2d4 => 2 * (d4 + d4)
    # We don't use brackets when not necessary
    # e.g 2+2d4 => 2 + d4 + d4
    # Minus in-front causes brackets
    # e.g 2-2d4 => 2 - (d4 + d4)

    result_string = ""
    current_substring = ""

    # Key is the letter to denote using that operation for the dice rolls
    # First value is python syntax for that operation
    # Second and third values are operations that if precede or follow the roll mean the roll does not need brackets

    def eval_poly_roll(_string, op_letter="d", force_no_brackets=False):
        n, poly = check_letter(op_letter, _string)
        poly = int(poly)
        printv(n, op_letter, poly, force_no_brackets, verbose_flag=debug)
        _, result = roll_poly_n(poly, n)
        return_string = letter_operator_dict[op_letter][0].join(str(x) for x in result)

        if auto_bracket_dice and not force_no_brackets:
            return_string = "(" + return_string + ")"

        return return_string

    def eval_substring(_string, next_char, prev_char):
        # Check we only have one operation in our string
        operations = 0
        operator_letter = None
        for _char in _string:
            if _char in letter_operator_dict.keys():
                operations += 1
                operator_letter = _char
        if operations > 1:
            raise Exception(f"Multiple operations found in {_string} from roll_string {roll_string}")

        # If we have an operator we need to evaluate it
        if operator_letter is not None:

            # Check whether to force no brackets, remember char is character after our current substring
            single_roll = _string[0] in letter_operator_dict.keys()

            # The following logic figures out if the surroundings of the roll mean it doesn't need brackets
            v = letter_operator_dict[operator_letter]
            valid_pre_chars = v[1] + [""]
            valid_next_chars = v[2] + [""]
            good_environment = (next_char in valid_next_chars) and (prev_char in valid_pre_chars)

            fnb = single_roll or good_environment

            # Evaluate the substring
            return eval_poly_roll(_string, op_letter=operator_letter, force_no_brackets=fnb)

        else:
            return _string

    for char in roll_string:
        if char.isdigit() or char in letter_operator_dict.keys():
            current_substring += char

        else:
            if current_substring != "":
                p_char = "" if result_string == "" else result_string[-1]
                result_string += eval_substring(current_substring, next_char=char, prev_char=p_char)
                current_substring = ""

            result_string += char

    # Evaluate trailing substring
    if current_substring != "":
        p_char = "" if result_string == "" else result_string[-1]
        n_char = ""
        result_string += eval_substring(current_substring, next_char=n_char, prev_char=p_char)

    # Evaluate our final expression
    value = eval(result_string)

    # Check if our result_string was more than just a number and if so correct it
    just_number = True
    for char in result_string:
        just_number = char.isdigit() and just_number
    if not just_number:
        result_string = f"{value} ({result_string})"

    printv(result_string, verbose_flag=debug)

    return value, result_string


def roll_core_old(roll_string, debug=False):
    roll_core_result = []
    roll_string_elements = roll_string.split('+')

    for roll_string_element in roll_string_elements:
        if 'd' in roll_string_element:
            n, poly = check_letter("d", roll_string_element)
            poly = int(poly)
            printv(n, poly, verbose_flag=debug)
            _, result = roll_poly_n(poly, n)

        else:
            result = [int(roll_string_element)]
            printv(result, verbose_flag=debug)

        roll_core_result.extend(result)

    value = sum(roll_core_result)
    result_string = f"{value} ({'+'.join(str(x) for x in roll_core_result)})" if len(roll_core_result) > 1 else str(value)

    return value, result_string


def roll(roll_strings, debug=False):
    """
    Takes a string and produces a list of rolls from that string
    Rolls are either just the values or a dict of value and breakdown

    :param debug: Whether or not print internal outputs of rolls for debugging
    :param roll_strings: The strings to roll from

        General roll_string: tnla
            a: The general roll string. ydz will roll a polyhedral dice with sides z, y times.
                When using d, the dice rolls are summed, to multiply them use p instead.
                If y is not given it is assumed to be 1, d and p function identically in this case.
                Other than that everything is evaluated by eval
            nl: Optional, Whether to make multiple rolls of a. n is the number of rolls.
            t: Optional, for use with nl. Including it will sum the total output of that string
                omitting it will print each l roll separately

        Conditional roll_string: tnlv>c:t'?f'
            tnl are as before, they apply to the roll as a whole. (Leading t only)
            v: The first roll string
            c: The second roll string
            >: The condition for roll strings v and c. Evaluated with python (So use == not = for equals)
            t': The roll string to be evaluated if condition evaluated to true
            f': The roll string to be evaluated if condition evaluated to false

            v, c, t, and f can be used in the expressions of t' and f' to use those results.
            Cannot use t in t' or f in f'.
            Cannot use both t in f' and f in t'.
            If no ?f' provided, assumes ?0
            For example, roll a d20 and if above 16 roll a d8 else use half the d8 value:
                d20>16:d8?t/2

        Fixed values and arrays:
            If multiple lots of a roll_string are being evaluated (i.e n>1 for nl) then we can fix and array rolls
            Fixed rolls are calculated once and their value is substituted for all the resulting rolls
                NOTE: Only the final value of a fixed roll will appear in the 'roll explanation'
            Arrays can be used to have slightly different rolls across the multiple rolls
                Array elements MUST equal the number of rolls for that string, arrays can't be nested or overlap.

            xydz is the syntax for fixed rolls where x denotes fixed and as before ydz means roll polyhedral with z
                sides, y times and sum the result. Using p instead of d multiplies the result.

            [a1,a2,...] is the general form of an array where a1, a2, etc. are different roll_strings for each roll.
                Example: 3ld20+[3,5,6], equivalent to d20+3 d20+5 d20+6


    Example Outputs:
        d8
            6

        2d20+4
            24 (16+8+4)

        2ld20+4
            14 (10+4)
            8 (4+4)

        t2ld20+4
            22 [14 (10+4), 8 (4+4)]

        d20+8>16:d8+4?0    (Note - if no ? provided, assumes ?0)
            6 {17 (9+8) > 16: 6 (2+4)}

        t2ld20>16:d4
            3 [3 {17 > 16: 3}, 0 {12 > 16: 0}]
    """

    # First we break up our roll strings into the individual sets of rolls
    roll_strings = ensure_list(roll_strings)
    roll_strings = flatten([roll_string.split(' ') for roll_string in roll_strings])

    # List of dicts to store our rolls
    rolls = []

    # Iterate through roll_strings and perform rolls
    for roll_string in roll_strings:

        # Check string only has expected characters in it
        for character in roll_string:
            if not character.isdigit() and character not in legal_characters:
                raise Exception(f"Illegal character, {character}, in roll string {roll_string}. Legal characters: {legal_characters}")

        # Check if we are in total mode
        t = roll_string[0] == "t"
        totalled_multi_roll = ["t"]
        if t:
            roll_string = roll_string[1:]

        # Check if we have to do multiple lots of this roll (i.e. nl...)
        l, roll_string_core_base = check_letter("l", roll_string)
        printv(l, roll_string_core_base, verbose_flag=debug)

        # Run through roll_string_core_base and evaluate any fixed rolls or sections
        if 'x' in roll_string_core_base:  # Just to avoid checking if we don't need to
            found_an_x = False
            found_an_operator = False
            open_brackets = 0
            new_roll_string_core_base = ''
            current_substring = ''

            def eval_fixed_roll_string(_string):
                nonlocal new_roll_string_core_base
                x_result, _ = roll_core(_string, debug=debug)
                new_roll_string_core_base += str(x_result)

            for char in roll_string_core_base:
                if char == 'x':
                    # Checking for max
                    if len(current_substring) > 1 and current_substring[-2:] == 'ma':
                        current_substring += 'x'
                    elif current_substring == '' and len(new_roll_string_core_base) > 1 and new_roll_string_core_base[-2:] == 'ma':
                        new_roll_string_core_base += 'x'

                    elif not found_an_x:
                        found_an_x = True

                    else:
                        raise Exception(f"Nested x's found in {roll_string}")

                elif char in letter_operator_dict.keys() and found_an_x:
                    current_substring += char
                    found_an_operator = True

                elif not char.isdigit() and found_an_operator and open_brackets == 0 and found_an_x:
                    eval_fixed_roll_string(current_substring)
                    new_roll_string_core_base += char
                    current_substring = ''
                    found_an_x = False

                elif char == '(' and found_an_x:
                    open_brackets += 1
                    current_substring += char

                elif char == ')' and found_an_x:
                    open_brackets += -1
                    current_substring += char

                    if open_brackets == 0:
                        eval_fixed_roll_string(current_substring)
                        current_substring = ''
                        found_an_x = False

                    if open_brackets < 0:
                        raise Exception(f"Found a wrong way bracket in {roll_string}")

                elif found_an_x:
                    current_substring += char

                else:
                    new_roll_string_core_base += char
                    if current_substring != '':
                        raise Exception('Oh no the code is broken')

            if found_an_x:  # We reached end of string before we had to evaluate the fixed portion
                if open_brackets != 0:
                    raise Exception(f"{roll_string} ended with open brackets after an x was found")
                eval_fixed_roll_string(current_substring)

            roll_string_core_base = new_roll_string_core_base

        # Create a list of the roll_string_cores to iterate through
        if '[' in roll_string_core_base:
            # This will hold our roll_strings as we build them up
            roll_string_cores = [''] * l
            in_an_array = False
            current_substring = ''

            for char in roll_string_core_base:
                if char == '[' and not in_an_array:
                    roll_string_cores = [x + current_substring for x in roll_string_cores]
                    current_substring = ''
                    in_an_array = True

                elif char == ']' and in_an_array:
                    parts = current_substring.split(',')

                    # Check array size matches l
                    if len(parts) != l:
                        raise Exception(f"Size of array [{current_substring}] in roll_string {roll_string} does not match {l}, the number of rolls")

                    # Add each part to a different roll_string_core
                    for i, part in enumerate(parts):
                        roll_string_cores[i] += part

                    current_substring = ''
                    in_an_array = False

                elif char in ['[', ']']:  # If we hit a sq bracket we shouldn't have
                    raise Exception(f"Square bracket issues in roll string {roll_string}")

                else:
                    current_substring += char

            # If last character was not a ], we need to make sure the end of the roll_string gets added to our cores
            if current_substring != '':
                roll_string_cores = [x + current_substring for x in roll_string_cores]

        else:
            roll_string_cores = [roll_string_core_base] * l

        # Iterate through each roll_string_core
        for roll_string_core in roll_string_cores:

            # Conditional roll handling
            if ":" in roll_string_core:

                # We'll split roll into 4 parts - Value, Condition, True, False
                # We'll also save our comparator (>, <, =, etc...)

                # Split value & condition from true & false
                parts = roll_string_core.split(':')

                # Input default false condition
                if "?" not in parts[1]:
                    parts[1] += "?0"

                # Split true and false
                parts[1] = parts[1].split("?")

                # Get comparator
                comparators = ["<=", ">=", "<", ">", "==", "!="]
                for _comparator in comparators:
                    if _comparator in parts[0]:
                        parts[0] = parts[0].split(_comparator)
                        comparator = _comparator
                        break
                else:
                    raise Exception(f"Comparator not found in conditional roll string {roll_string_core}")

                # Flatten down to 4 element list
                parts = flatten(parts)

                # Now we need to evaluate things
                # Other values can be called in the true or false expressions via v, c, t, f
                value = roll_core(parts[0], debug=debug)
                condition = roll_core(parts[1], debug=debug)

                # Since we want to allow the use of 'int' in roll strings we'll need to make sure that t doesn't get interpreted wrong
                # We'll remove it now and add it back later as a fix
                parts[2] = parts[2].replace("int", "inx")
                parts[3] = parts[3].replace("int", "inx")

                # We need to figure out which way to evaluate true and false in case they call on each other
                # First we check nothing illegal is happening
                if ("t" in parts[3] and "f" in parts[2]) or ("t" in parts[2]) or ("f" in parts[3]):
                    raise Exception(f"Illegal use of t or f in roll_string_core {roll_string_core}")

                parts[2] = parts[2].replace("v", str(value[0]))
                parts[2] = parts[2].replace("c", str(condition[0]))
                parts[3] = parts[3].replace("v", str(value[0]))
                parts[3] = parts[3].replace("c", str(condition[0]))

                if "f" in parts[2]:
                    parts[3] = parts[3].replace("inx", "int")
                    false_result = roll_core(parts[3], debug=debug)
                    parts[2] = parts[2].replace("f", str(false_result[0]))
                    parts[2] = parts[2].replace("inx", "int")
                    true_result = roll_core(parts[2], debug=debug)
                elif "t" in parts[3]:
                    parts[2] = parts[2].replace("inx", "int")
                    true_result = roll_core(parts[2], debug=debug)
                    parts[3] = parts[3].replace("t", str(true_result[0]))
                    parts[3] = parts[3].replace("inx", "int")
                    false_result = roll_core(parts[3], debug=debug)
                else:
                    false_result = roll_core(parts[3], debug=debug)
                    true_result = roll_core(parts[2], debug=debug)
                    parts[2] = parts[2].replace("inx", "int")
                    parts[3] = parts[3].replace("inx", "int")

                # Evaluate the comparison
                comparison_string = f"{value[0]} {comparator} {condition[0]}"
                if eval(comparison_string):
                    result = true_result
                else:
                    result = false_result

                result_value = result[0]
                result_string = f"{result_value} {{{value[1]} {comparator} {condition[1]}: {result[1]}}}"

                roll_core_result = (result_value, result_string)
                printv(roll_core_result, verbose_flag=debug)

            else:
                roll_core_result = roll_core(roll_string_core, debug=debug)
                printv(roll_core_result, verbose_flag=debug)

            if t:
                totalled_multi_roll.append(roll_core_result)
            else:
                rolls.append(roll_core_result)

        if t:
            rolls.append(totalled_multi_roll)

    # Arrange our rolls into an output where each element is a roll list with result_value and result_string as elements
    output = []
    for _roll in rolls:
        # Check if it's a totalled_multi_roll
        if _roll[0] == "t":
            sub_rolls = _roll[1:]
            total = sum(sub_roll[0] for sub_roll in sub_rolls)
            result_string = f"{total} [{', '.join(sub_roll[1] for sub_roll in sub_rolls)}]"
            output.append([total, result_string])
        else:
            output.append(_roll)

    return output


def print_roll(roll_strings, debug=False):
    rolls = roll(roll_strings, debug=debug)
    for _roll in rolls:
        if _roll[1] is not None:
            print(_roll[1])


def roll_session(debug=False):
    while True:
        u_in = input("\n")
        if u_in.lower() in ["", "break", "stop", "exit"]:
            break
        else:
            print_roll(u_in, debug=debug)
